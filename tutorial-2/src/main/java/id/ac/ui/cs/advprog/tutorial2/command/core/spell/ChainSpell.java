package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    protected ArrayList<Spell> spell_list;
    // TODO: Complete Me

    public ChainSpell(ArrayList<Spell> spell_list) {
        this.spell_list = spell_list;
    }

    @Override
    public void cast() {
        for(Spell x : spell_list) {
            x.cast();
        }
    }

    @Override
    public void undo() {
        for(int i = spell_list.size()-1; i > -1; i--) {
            spell_list.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
