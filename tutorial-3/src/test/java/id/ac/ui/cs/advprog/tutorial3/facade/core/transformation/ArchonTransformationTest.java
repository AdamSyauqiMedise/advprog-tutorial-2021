package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ArchonTransformationTest {
    private Class<?> archonClass;

    @BeforeEach
    public void setup() throws Exception {
        archonClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.ArchonTransformation");
    }

    @Test
    public void testArchonHasEncodeMethod() throws Exception {
        Method translate = archonClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testArchonEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "WejmveDerhDMD0irxDxsDeDfpegowqmxlDxsDjsvkiDsyvDw0svh";

        Spell result = new ArchonTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testArchonHasDecodeMethod() throws Exception {
        Method translate = archonClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testArchonDecodesCorrectly() throws Exception {
        String text = "WejmveDerhDMD0irxDxsDeDfpegowqmxlDxsDjsvkiDsyvDw0svh";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new ArchonTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }
}
