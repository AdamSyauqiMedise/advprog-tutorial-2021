package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;

// TODO: complete me :)
public class BowAdapter implements Weapon {

    private Bow bow;
    private boolean isAimed;

    public BowAdapter(Bow bow) {
        this.bow = bow;
    }

    @Override
    public String normalAttack() {
        System.out.println(isAimed);
        return bow.shootArrow(isAimed);
    }

    @Override
    public String chargedAttack() {
        if(isAimed == true) {
            this.isAimed = false;
            System.out.println(isAimed);
            return "switching to quick fire mode";
        } else if (isAimed == false) {
            this.isAimed = true;
            System.out.println(isAimed);
            return "switching to aimed fire mode";
        }
        return null;
    }


    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        return bow.getHolderName();
    }
}
