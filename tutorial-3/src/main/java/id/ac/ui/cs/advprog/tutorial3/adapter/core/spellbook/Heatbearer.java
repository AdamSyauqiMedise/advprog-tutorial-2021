package id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook;


public class Heatbearer implements Spellbook {

    private String holderName;

    public Heatbearer(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String smallSpell() {
        return holderName + " scarred the enemy using " + getName();
    }

    @Override
    public String largeSpell() {
        return holderName + " *breaths EXPUULOOOOSHHHIOONNNN! (using) " + getName();
    }

    @Override
    public String getName() {
        return "Heat Bearer";
    }

    @Override
    public String getHolderName() { return holderName; }
}