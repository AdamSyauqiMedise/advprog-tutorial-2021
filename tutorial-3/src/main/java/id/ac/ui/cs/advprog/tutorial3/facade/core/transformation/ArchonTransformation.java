package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;

public class ArchonTransformation {
    private static int shift;

    public ArchonTransformation() {
        shift = 4;
    }

    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode) {
        int selector = encode ? 1 : -1;
        Codex codex = spell.getCodex();
        int codexSize = codex.getCharSize();
        char[] result = spell.getText().toCharArray();
        for(int i = 0; i < result.length; i++) {
            int j = codex.getIndex(result[i]) + (shift * selector);
            if(j < 0) {
                j += codexSize;
            } else {
                j %= codexSize;
            }
            result[i] = codex.getChar(j);
            System.out.println(result[i]);
        }
        return new Spell(new String(result), codex);
    }
}
