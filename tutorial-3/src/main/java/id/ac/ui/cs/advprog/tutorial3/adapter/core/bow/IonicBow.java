package id.ac.ui.cs.advprog.tutorial3.adapter.core.bow;

public class IonicBow implements Bow {

    private String holderName;

    public IonicBow(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String shootArrow(boolean isAimShot) {
        if(isAimShot) {
            return holderName + "'s Arrow reacted with the enemy's protons using " + getName();
        } else {
            return holderName + " Separated one atom from the enemy using " + getName();
        }
    }

    @Override
    public String getName() {
        return "Ionic Bow";
    }

    @Override
    public String getHolderName() { return holderName; }
}