package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    private boolean list_ready = false;

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        if(!list_ready) {
            for(Spellbook spellbook: spellbookRepository.findAll()) {
                weaponRepository.save(new SpellbookAdapter(spellbook));
            }
            for(Bow bow: bowRepository.findAll()) {
                weaponRepository.save(new BowAdapter(bow));
            }
            list_ready = true;
            return weaponRepository.findAll();
        } else {
            return weaponRepository.findAll();
        }

    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon curr_weapon = this.weaponRepository.findByAlias(weaponName);
        weaponRepository.save(curr_weapon);
        if(attackType == 0) {
            logRepository.addLog(curr_weapon.normalAttack());
        } else if(attackType == 1) {
            logRepository.addLog(curr_weapon.chargedAttack());
        }
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return this.logRepository.findAll();
    }
}
