package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean energy;

    public SpellbookAdapter(Spellbook spellbook) {
        this.energy = true;
        this.spellbook = spellbook;
    }

    @Override
    public String normalAttack() {
        rechargeEnergy();
        System.out.println(this.energy);
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if(getEnergy() == true) {
            useEnergy();
            System.out.println("Energy full");
            return spellbook.largeSpell();
        } else {
            System.out.println("No Energy");
            return "Can't cast two charged spell two times in a row";
        }
    }

    public void useEnergy() {
        this.energy = false;
    }

    public void rechargeEnergy() {
        this.energy = true;
    }

    public boolean getEnergy() {
        System.out.println(this.energy);
        return energy;
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
