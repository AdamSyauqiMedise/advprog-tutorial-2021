package id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook;

public class TheWindjedi implements Spellbook {

    private String holderName;

    public TheWindjedi(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String smallSpell() {
        return holderName + " launched Small Musical Attack using " + getName();
    }

    @Override
    public String largeSpell() {
        return holderName + " launched Orchestra-class musical attack using " + getName();
    }

    @Override
    public String getName() {
        return "The Windjedi";
    }

    @Override
    public String getHolderName() { return holderName; }
}