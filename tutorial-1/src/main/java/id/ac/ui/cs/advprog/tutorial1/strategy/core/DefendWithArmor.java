package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {

    @Override
    public String defend() {
        return "Nobody can get through this armor!";
    }

    @Override
    public String getType() {
        return "Mark 50 Armor";
    }
    //ToDo: Complete me
}
