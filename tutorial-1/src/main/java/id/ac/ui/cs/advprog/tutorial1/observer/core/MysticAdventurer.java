package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild = guild;
        //ToDo: Complete Me
    }

    @Override
    public void update() {
        Quest quest = guild.getQuest();
        String quest_type = guild.getQuestType();
        if(quest_type.equals("D")) {
            getQuests().add(quest);
        } else if(quest_type.equals("E")){
            getQuests().add((quest));
        }
    }

    //ToDo: Complete Me
}
