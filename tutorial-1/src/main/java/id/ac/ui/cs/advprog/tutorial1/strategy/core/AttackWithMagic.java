package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {

    @Override
    public String attack() {
        return "Roll 1d4 + 1";
    }

    @Override
    public String getType() {
        return "Magic Missile";
    }
    //ToDo: Complete me
}
