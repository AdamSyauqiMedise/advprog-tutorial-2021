package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {

    @Override
    public String defend() {
        return "Zhongli uses E Hold";
    }

    @Override
    public String getType() {
        return "Cast Mage Shield";
    }
    //ToDo: Complete me
}
