package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {

    @Override
    public String defend() {
        return "The strongest metal on earth (broken by Thanos tho)";
    }

    @Override
    public String getType() {
        return "Captain America's Shield";
    }
    //ToDo: Complete me
}
