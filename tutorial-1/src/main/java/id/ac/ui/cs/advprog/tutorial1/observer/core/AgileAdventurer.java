package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
        //ToDo: Complete Me
    }

    @Override
    public void update() {
        Quest quest = guild.getQuest();
        String quest_type = guild.getQuestType();
        if(quest_type.equals("D")) {
            getQuests().add(quest);
        } else if(quest_type.equals("R")){
            getQuests().add((quest));
        }
    }

    //ToDo: Complete Me
}
