package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.*;
import id.ac.ui.cs.advprog.tutorial1.observer.repository.QuestRepository;
import id.ac.ui.cs.advprog.tutorial1.observer.core.Adventurer;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GuildServiceImpl implements GuildService {
    private final QuestRepository questRepository;
    private final Guild guild;
    private final Adventurer agileAdventurer;
    private final Adventurer knightAdventurer;
    private final Adventurer mysticAdventurer;

    public GuildServiceImpl(QuestRepository questRepository) {
        this.questRepository = questRepository;
        this.guild = new Guild();
        this.agileAdventurer = new AgileAdventurer(guild);
        this.knightAdventurer = new KnightAdventurer((guild));
        this.mysticAdventurer = new MysticAdventurer((guild));
        this.guild.add(agileAdventurer);
        this.guild.add(knightAdventurer);
        this.guild.add(mysticAdventurer);
        //ToDo: Complete Me
    }

    @Override
    public void addQuest(Quest quest) {
        questRepository.save(quest);
        guild.addQuest(quest);
        System.out.println(quest.getTitle());
        System.out.println("Service");
    }

    @Override
    public List<Adventurer> getAdventurers() {
        List<Adventurer> listAdventurer = new ArrayList<>();
        listAdventurer.add(agileAdventurer);
        listAdventurer.add(knightAdventurer);
        listAdventurer.add(mysticAdventurer);
        return listAdventurer;
    }

    //ToDo: Complete Me
}
